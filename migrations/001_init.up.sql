CREATE SCHEMA financial_claim_request_db;

CREATE TABLE financial_claim_request_db.configurations(
    token text NOT NULL,
    bsn varchar(9) NOT NULL,
    created_at timestamptz NOT NULL,
    app_public_key text NOT NULL,
    PRIMARY KEY (token)
);

