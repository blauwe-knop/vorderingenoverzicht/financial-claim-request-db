# financial-claim-request-db Helm chart

By default, this chart uses a cloud-native postgresql setup. To use an external database instead, 
simply set `pgCloudNativeEnabled: false`.

## Postgresql secret
The cloud-native postgresql setup creates a secret like the following:
```
Name:         financial-claim-request-db-app
Namespace:    bk-source-organization-manager
Labels:       cnpg.io/cluster=financial-claim-request-db
              cnpg.io/reload=true
Annotations:  cnpg.io/operatorVersion: 1.22.0

Type:  kubernetes.io/basic-auth

Data
====
username:  26 bytes
host:      29 bytes
jdbc-uri:  185 bytes
password:  64 bytes
pgpass:    154 bytes
port:      4 bytes
user:      26 bytes
dbname:    26 bytes
```
When disabling cloud-native postgresql, be sure to manually create a secret like above, 
because "username" and "password" are still being used.